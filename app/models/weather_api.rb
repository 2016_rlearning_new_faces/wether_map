class WeatherApi
  require 'rexml/document'
  require "net/http"
  require "json"
  require "uri"
  require 'open-uri'
  require "date"

  def self.apidate
    uri  = URI.parse("http://api.openweathermap.org/data/2.5/weather")
    json = Net::HTTP.get(uri+ "?q=Tokyo,jp&APPID=520d334e2119dc072bb4d23882fc3fba")
    JSON.parse(json)
  end
  
  def self.weather_id #天気データの抽出
    weather = WeatherApi.apidate 
    weather["weather"][0]["id"] #天気のidを抽出
  end

  def weather_status  #天気ステータスの判定
    weather_id = WeatherApi.weather_id
    case weather_id
    when 200,201,202,210,211,212,221,230,231,232
      "雷雨"
    when 300,301,302,310,311,312,313,314,321
      "霧雨"
    when 500
      "小雨"
    when 501
      "雨"
    when 502
      "強い雨"
    when 503
      "豪雨"
    when 504
      "極端に強い雨"
    when 511,520,521,522,531
      "シャワーのような雨"
    when 600
      "小雪"
    when 601
      "雪"
    when 602
      "大雪"
    when 611,612
      "みぞれ"
    when 615,616
      "雨と雪が降る"
    when 620,621,622
      "シャワーのような雪"
    when 701,721,741
      "霧"
    when 731,751
      "砂塵が舞う"
    when 761,711
      "ほこりっぽい"
    when 762
      "火山灰"
    when 771
      "スコール"
    when 781,900,902
      "竜巻"
    when 800
      "晴れ"
    when 801,802,803,804
      "曇り"
    when 901
      "熱帯暴雨"
    when 903
      "かなり冷え込む"
    when 904
      "かなり暑い"
    when 905
      "強風"
    when 906
      "ひょうが降る"
    when 951
      "比較的に大気の状態が穏やか"
    when 952,953
      "風が吹き荒れる"
    when 954,955,956,957,958,959
      "暴風"
    when 960,961
      "嵐"
    when 962
      "ハリケーン"
    else
      "現在の天気情報を取得できません"
    end
  end

  
  def weather_icon
    weather = WeatherApi.apidate
    icon = weather["weather"][0]["icon"] #天気のアイコン画像表示
    "http://openweathermap.org/img/w/#{icon}.png " #天気のアイコン表示用変数 
  end

  def temp_max
    weather = WeatherApi.apidate 
    t_max = weather["main"]["temp_max"]  #気温のデータを一時的に格納  
    t_max - 273.15
  end
  
  def temp_min
    weather = WeatherApi.apidate
    t_min = weather["main"]["temp_min"] #気温のデータを一時的に格納
    t_min - 273.15 
  end
   
  def rain_fallchance
    doc = REXML::Document.new(open("http://www.drk7.jp/weather/xml/13.xml"))
    date = doc.elements["weatherforecast/pref/area/info/rainfallchance"]
    p1 = date.elements["period[1]"].text #0時から6時
    p2 = date.elements["period[2]"].text #6時から12時
    p3 = date.elements["period[3]"].text #12時から18時
    p4 = date.elements["period[4]"].text #18時から24時
    rain_days = p1,p2,p3,p4
    #降水確率平均値計算
    rain_all = rain_days.map(&:to_i).inject {|sum, n| sum + n }
    rain_all / 4  
  end
end
